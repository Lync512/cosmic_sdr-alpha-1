# Cosmic_SDR Alpha 1

Cosmic_SDR is an new SDR radio based on the Analog Devices ADRV9009 transceiver IC and designed in KiCAD.
Every aspect of the SDR from the KiCAD files, to the software will be open sourced and freely available. 

With my initial alpha 1 post I have the schematics for the ADRV9009 IC in KiCAD, which are currently incomplete.
The next step is to add a FPGA/SOC to the schematics and complete the shcematic file. 

More to come, stay tuned. 