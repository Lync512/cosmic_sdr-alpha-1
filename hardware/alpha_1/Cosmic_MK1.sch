EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "Cosmic_SDR"
Date "2019-08-15"
Rev "Mk. 1"
Comp "ASTREX Space Lab"
Comment1 "By Lync"
Comment2 ""
Comment3 ""
Comment4 "49394D 6554 576552 57495448 47546556495459 "
$EndDescr
$Sheet
S 650  650  1500 1500
U 5DA5334A
F0 "ADVR9009_A" 50
F1 "ADVR9009_A.sch" 50
$EndSheet
$Sheet
S 650  2500 1500 1500
U 5DA53393
F0 "ADRV9009_B" 50
F1 "ADRV9009_B.sch" 50
$EndSheet
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5D5EFFD1
P 750 7600
F 0 "#LOGO1" H 750 7875 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 750 7375 50  0001 C CNN
F 2 "" H 750 7600 50  0001 C CNN
F 3 "~" H 750 7600 50  0001 C CNN
	1    750  7600
	1    0    0    -1  
$EndComp
$Sheet
S 2500 2500 1500 1500
U 5D83997B
F0 "Clock_gen" 49
F1 "Clock_gen.sch" 49
$EndSheet
$Sheet
S 2500 650  1500 1500
U 5D782CC1
F0 "POWER" 50
F1 "POWER.sch" 50
$EndSheet
$Sheet
S 2500 4350 1500 1500
U 5D7867BD
F0 "TX" 49
F1 "TX.sch" 49
$EndSheet
$Sheet
S 650  4350 1500 1500
U 5D5D21F2
F0 "RX" 50
F1 "RX.sch" 50
$EndSheet
$Sheet
S 4350 650  1500 1500
U 5E0D667E
F0 "FPGA_CONNECT" 50
F1 "FPGA_CONNECT.sch" 50
$EndSheet
$EndSCHEMATC
