EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 8
Title "AXSDR: COSMIC_RF"
Date "2019-08-15"
Rev "Mk. 1"
Comp "ASTREX Space Lab"
Comment1 "By Lync"
Comment2 ""
Comment3 ""
Comment4 "49394D 6554 576552 57495448 47546556495459 "
$EndDescr
$Comp
L ADRV9009:ADRV9009BBCZ U1
U 1 1 5DA53427
P 5700 1700
AR Path="/5DA5334A/5DA53427" Ref="U1"  Part="1" 
AR Path="/5E0CFB1B/5DA53427" Ref="U?"  Part="1" 
F 0 "U1" H 7200 2087 60  0000 C CNN
F 1 "ADRV9009BBCZ" H 7200 1981 60  0000 C CNN
F 2 "BC_196_13" H 7200 1940 60  0001 C CNN
F 3 "" H 5700 1700 60  0000 C CNN
	1    5700 1700
	1    0    0    -1  
$EndComp
Text GLabel 5700 1700 0    50   Input ~ 0
RX1_IN+
Text GLabel 5700 1800 0    50   Input ~ 0
RX1_IN-
Text GLabel 5700 1900 0    50   Input ~ 0
RX2_IN+
Text GLabel 5700 2000 0    50   Input ~ 0
RX2_IN-
Text GLabel 5700 3000 0    50   Output ~ 0
TX1_OUT+
Text GLabel 5700 3100 0    50   Output ~ 0
TX1_OUT-
Text GLabel 5700 3200 0    50   Output ~ 0
TX2_OUT+
Text GLabel 5700 3300 0    50   Output ~ 0
TX2_OUT-
Text GLabel 5700 3500 0    50   BiDi ~ 0
GPIO_3P3_0
Text GLabel 5700 3600 0    50   BiDi ~ 0
GPIO_3P3_1
Text GLabel 5700 3700 0    50   BiDi ~ 0
GPIO_3P3_2
Text GLabel 5700 3800 0    50   BiDi ~ 0
GPIO_3P3_3
Text GLabel 5700 3900 0    50   BiDi ~ 0
GPIO_3P3_4
Text GLabel 5700 4000 0    50   BiDi ~ 0
GPIO_3P3_5
Text GLabel 5700 4100 0    50   BiDi ~ 0
GPIO_3P3_6
Text GLabel 5700 4200 0    50   BiDi ~ 0
GPIO_3P3_7
Text GLabel 5700 4300 0    50   BiDi ~ 0
GPIO_3P3_8
Text GLabel 5700 4400 0    50   BiDi ~ 0
GPIO_3P3_9
Text GLabel 5700 4500 0    50   BiDi ~ 0
GPIO_3P3_10
Text GLabel 5700 4600 0    50   BiDi ~ 0
GPIO_3P3_11
Text GLabel 5700 4800 0    50   BiDi ~ 0
GPIO_0
Text GLabel 5700 4900 0    50   BiDi ~ 0
GPIO_1
Text GLabel 5700 5000 0    50   BiDi ~ 0
GPIO_2
Text GLabel 5700 5100 0    50   BiDi ~ 0
GPIO_3
Text GLabel 5700 5200 0    50   BiDi ~ 0
GPIO_4
Text GLabel 5700 5300 0    50   BiDi ~ 0
GPIO_5
Text GLabel 5700 5400 0    50   BiDi ~ 0
GPIO_6
Text GLabel 5700 5500 0    50   BiDi ~ 0
GPIO_7
Text GLabel 5700 5600 0    50   BiDi ~ 0
GPIO_8
Text GLabel 5700 5700 0    50   BiDi ~ 0
GPIO_9
Text GLabel 5700 5800 0    50   BiDi ~ 0
GPIO_10
Text GLabel 5700 5900 0    50   BiDi ~ 0
GPIO_11
Text GLabel 5700 6000 0    50   BiDi ~ 0
GPIO_12
Text GLabel 5700 6100 0    50   BiDi ~ 0
GPIO_13
Text GLabel 5700 6200 0    50   BiDi ~ 0
GPIO_14
Text GLabel 5700 6300 0    50   BiDi ~ 0
GPIO_15
Text GLabel 5700 6400 0    50   BiDi ~ 0
GPIO_16
Text GLabel 5700 6500 0    50   BiDi ~ 0
GPIO_17
Text GLabel 5700 6600 0    50   BiDi ~ 0
GPIO_18
Wire Wire Line
	4750 6800 4750 6900
Connection ~ 4750 6900
Wire Wire Line
	4750 6900 4750 7000
Connection ~ 4750 7000
NoConn ~ 5700 7300
Text GLabel 9100 2100 2    50   Input ~ 0
SYNCIN0+
Text GLabel 9100 2200 2    50   Input ~ 0
SYNCIN0-
Text GLabel 9100 2300 2    50   Input ~ 0
SYNC1+
Text GLabel 9100 2400 2    50   Input ~ 0
SYNC1-
NoConn ~ 9100 4700
NoConn ~ 9100 4600
NoConn ~ 9100 4500
NoConn ~ 9100 4400
NoConn ~ 9100 2600
NoConn ~ 9100 2700
NoConn ~ 9100 2800
NoConn ~ 9100 2900
NoConn ~ 9100 3000
NoConn ~ 9100 3100
NoConn ~ 9100 3200
NoConn ~ 9100 3300
NoConn ~ 9100 3500
NoConn ~ 9100 3600
NoConn ~ 9100 3700
NoConn ~ 9100 3800
NoConn ~ 9100 3900
NoConn ~ 9100 4000
NoConn ~ 9100 4100
NoConn ~ 9100 4200
Text GLabel 9100 4900 2    50   Input ~ 0
SYSREF_IN+
Text GLabel 9100 5000 2    50   Input ~ 0
SYSREF_IN-
NoConn ~ 9100 5200
Text GLabel 9100 6000 2    50   Input ~ 0
RESET
Text GLabel 9100 6200 2    50   Input ~ 0
SCLK
Text GLabel 9100 6300 2    50   Input ~ 0
SPI_CS0
Text GLabel 9100 5400 2    50   Input ~ 0
RX1_ENABLE
Text GLabel 9100 5500 2    50   Input ~ 0
RX2_ENABLE
Text GLabel 9100 5700 2    50   Input ~ 0
TX1_ENABLE
Text GLabel 9100 5800 2    50   Input ~ 0
TX2_ENABLE
Text GLabel 9100 6400 2    50   Output ~ 0
SPI_DOUT_DUT
Text GLabel 9100 6500 2    50   Input ~ 0
SPI_DIN
$Comp
L Device:C C3
U 1 1 5E010F92
P 10200 7200
AR Path="/5DA5334A/5E010F92" Ref="C3"  Part="1" 
AR Path="/5E0CFB1B/5E010F92" Ref="C?"  Part="1" 
F 0 "C3" V 9948 7200 50  0000 C CNN
F 1 "0.1UF" V 10039 7200 50  0000 C CNN
F 2 "" H 10238 7050 50  0001 C CNN
F 3 "~" H 10200 7200 50  0001 C CNN
	1    10200 7200
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5E011636
P 9800 6700
AR Path="/5DA5334A/5E011636" Ref="C2"  Part="1" 
AR Path="/5E0CFB1B/5E011636" Ref="C?"  Part="1" 
F 0 "C2" V 9548 6700 50  0000 C CNN
F 1 "0.1UF" V 9639 6700 50  0000 C CNN
F 2 "" H 9838 6550 50  0001 C CNN
F 3 "~" H 9800 6700 50  0001 C CNN
	1    9800 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	9100 6700 9650 6700
$Comp
L Device:R R2
U 1 1 5E026B39
P 10500 6950
AR Path="/5DA5334A/5E026B39" Ref="R2"  Part="1" 
AR Path="/5E0CFB1B/5E026B39" Ref="R?"  Part="1" 
F 0 "R2" H 10570 6996 50  0000 L CNN
F 1 "100" H 10570 6905 50  0000 L CNN
F 2 "" V 10430 6950 50  0001 C CNN
F 3 "~" H 10500 6950 50  0001 C CNN
	1    10500 6950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E02FFB3
P 9850 7450
AR Path="/5DA5334A/5E02FFB3" Ref="R1"  Part="1" 
AR Path="/5E0CFB1B/5E02FFB3" Ref="R?"  Part="1" 
F 0 "R1" H 9920 7496 50  0000 L CNN
F 1 "14.3K" H 9920 7405 50  0000 L CNN
F 2 "" V 9780 7450 50  0001 C CNN
F 3 "~" H 9850 7450 50  0001 C CNN
	1    9850 7450
	1    0    0    -1  
$EndComp
Text GLabel 11000 6700 2    50   Input ~ 0
REF_CLK_IN-
Text GLabel 11000 7200 2    50   Input ~ 0
REF_CLK_IN+
$Comp
L power:GNDA #PWR04
U 1 1 5E03A45E
P 9850 7750
AR Path="/5DA5334A/5E03A45E" Ref="#PWR04"  Part="1" 
AR Path="/5E0CFB1B/5E03A45E" Ref="#PWR?"  Part="1" 
F 0 "#PWR04" H 9850 7500 50  0001 C CNN
F 1 "GNDA" H 9855 7577 50  0000 C CNN
F 2 "" H 9850 7750 50  0001 C CNN
F 3 "" H 9850 7750 50  0001 C CNN
	1    9850 7750
	1    0    0    -1  
$EndComp
Text GLabel 9100 7200 2    50   Output ~ 0
RF_SYNTH_VTUNE
Wire Wire Line
	9850 7600 9850 7750
Wire Wire Line
	9100 7000 9850 7000
Wire Wire Line
	9850 7000 9850 7300
Wire Wire Line
	9100 6800 10050 6800
Wire Wire Line
	9950 6700 10500 6700
Wire Wire Line
	10050 6800 10050 7200
Wire Wire Line
	10350 7200 10500 7200
Wire Wire Line
	10500 6800 10500 6700
Connection ~ 10500 6700
Wire Wire Line
	10500 6700 11000 6700
Wire Wire Line
	10500 7100 10500 7200
Connection ~ 10500 7200
Wire Wire Line
	10500 7200 11000 7200
Text GLabel 5700 2200 0    50   Input ~ 0
ORX1_IN+
Text GLabel 5700 2300 0    50   Input ~ 0
ORX1_IN-
Text GLabel 5700 2400 0    50   Input ~ 0
ORX2_IN+
Text GLabel 5700 2500 0    50   Input ~ 0
ORX2_IN-
Text GLabel 9100 6100 2    50   Input ~ 0
TEST
$Comp
L power:GNDA #PWR01
U 1 1 5D66C436
P 4750 7250
AR Path="/5DA5334A/5D66C436" Ref="#PWR01"  Part="1" 
AR Path="/5E0CFB1B/5D66C436" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 4750 7000 50  0001 C CNN
F 1 "GNDA" H 4755 7077 50  0000 C CNN
F 2 "" H 4750 7250 50  0001 C CNN
F 3 "" H 4750 7250 50  0001 C CNN
	1    4750 7250
	1    0    0    -1  
$EndComp
Text GLabel 5700 7400 0    50   Output ~ 0
AUX_SYNTH_VTUNE
Wire Wire Line
	4750 6800 5700 6800
Wire Wire Line
	4750 7000 5700 7000
Wire Wire Line
	4750 6900 5700 6900
NoConn ~ 5700 2700
NoConn ~ 5700 2800
Wire Wire Line
	4750 7000 4750 7100
Wire Wire Line
	5700 7100 4750 7100
Connection ~ 4750 7100
Wire Wire Line
	4750 7100 4750 7250
$EndSCHEMATC
