EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 7 8
Title "AXSDR: COSMIC_RF"
Date "2019-12-30"
Rev "1"
Comp "ASTREX Space Lab"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "49394D 6554 576552 57495448 47546556495459 "
$EndDescr
$Comp
L Device:C C55
U 1 1 5E3A00EE
P 2050 5800
F 0 "C55" H 2165 5846 50  0000 L CNN
F 1 "C" H 2165 5755 50  0000 L CNN
F 2 "" H 2088 5650 50  0001 C CNN
F 3 "~" H 2050 5800 50  0001 C CNN
	1    2050 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR036
U 1 1 5E3A057F
P 2350 6350
F 0 "#PWR036" H 2350 6100 50  0001 C CNN
F 1 "GNDA" H 2355 6177 50  0000 C CNN
F 2 "" H 2350 6350 50  0001 C CNN
F 3 "" H 2350 6350 50  0001 C CNN
	1    2350 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C56
U 1 1 5E3A0884
P 2350 6000
F 0 "C56" H 2465 6046 50  0000 L CNN
F 1 "C" H 2465 5955 50  0000 L CNN
F 2 "" H 2388 5850 50  0001 C CNN
F 3 "~" H 2350 6000 50  0001 C CNN
	1    2350 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VDC #PWR035
U 1 1 5E3A0DC6
P 1850 5500
F 0 "#PWR035" H 1850 5400 50  0001 C CNN
F 1 "VDC" H 1850 5775 50  0000 C CNN
F 2 "" H 1850 5500 50  0001 C CNN
F 3 "" H 1850 5500 50  0001 C CNN
	1    1850 5500
	-1   0    0    1   
$EndComp
Text Label 1850 5500 2    50   ~ 0
VDDA1P3_ANLG
$Comp
L Connector:TestPoint TP1
U 1 1 5E3A3E1C
P 2850 5350
F 0 "TP1" H 2908 5468 50  0000 L CNN
F 1 "TestPoint" H 2908 5377 50  0000 L CNN
F 2 "" H 3050 5350 50  0001 C CNN
F 3 "~" H 3050 5350 50  0001 C CNN
	1    2850 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 5500 2050 5500
Wire Wire Line
	2850 5350 2850 5500
Connection ~ 2850 5500
Wire Wire Line
	2050 5650 2050 5500
Connection ~ 2050 5500
Wire Wire Line
	2050 5500 2350 5500
Wire Wire Line
	2350 5850 2350 5500
Connection ~ 2350 5500
Wire Wire Line
	2350 5500 2850 5500
Wire Wire Line
	2350 6150 2350 6200
Wire Wire Line
	2050 5950 2050 6200
Wire Wire Line
	2050 6200 2350 6200
Connection ~ 2350 6200
Wire Wire Line
	2350 6200 2350 6350
$Comp
L Connector:TestPoint TP2
U 1 1 5E3AB40E
P 2850 5850
F 0 "TP2" H 2908 5968 50  0000 L CNN
F 1 "TestPoint" H 2908 5877 50  0000 L CNN
F 2 "" H 3050 5850 50  0001 C CNN
F 3 "~" H 3050 5850 50  0001 C CNN
	1    2850 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR037
U 1 1 5E3AB71B
P 2850 6000
F 0 "#PWR037" H 2850 5750 50  0001 C CNN
F 1 "GNDA" H 2855 5827 50  0000 C CNN
F 2 "" H 2850 6000 50  0001 C CNN
F 3 "" H 2850 6000 50  0001 C CNN
	1    2850 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 5850 2850 6000
Wire Wire Line
	2850 5500 4850 5500
$Comp
L Device:R R32
U 1 1 5E3AE897
P 5700 2000
F 0 "R32" V 5907 2000 50  0000 C CNN
F 1 "0" V 5816 2000 50  0000 C CNN
F 2 "" V 5630 2000 50  0001 C CNN
F 3 "~" H 5700 2000 50  0001 C CNN
	1    5700 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 2000 5550 2000
$Comp
L Connector:TestPoint TP3
U 1 1 5E3AF510
P 6250 1900
F 0 "TP3" V 6353 1972 50  0000 C CNN
F 1 "TestPoint" H 6308 1927 50  0001 L CNN
F 2 "" H 6450 1900 50  0001 C CNN
F 3 "~" H 6450 1900 50  0001 C CNN
	1    6250 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C57
U 1 1 5E3AFE68
P 8700 3200
F 0 "C57" H 8815 3246 50  0000 L CNN
F 1 "220UF" H 8815 3155 50  0000 L CNN
F 2 "" H 8738 3050 50  0001 C CNN
F 3 "~" H 8700 3200 50  0001 C CNN
	1    8700 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C58
U 1 1 5E3B009B
P 9150 3200
F 0 "C58" H 9265 3246 50  0000 L CNN
F 1 "220UF" H 9265 3155 50  0000 L CNN
F 2 "" H 9188 3050 50  0001 C CNN
F 3 "~" H 9150 3200 50  0001 C CNN
	1    9150 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C59
U 1 1 5E3B01BF
P 9600 3200
F 0 "C59" H 9715 3246 50  0000 L CNN
F 1 "0.1UF" H 9715 3155 50  0000 L CNN
F 2 "" H 9638 3050 50  0001 C CNN
F 3 "~" H 9600 3200 50  0001 C CNN
	1    9600 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR038
U 1 1 5E3B04D8
P 9250 3550
F 0 "#PWR038" H 9250 3300 50  0001 C CNN
F 1 "GNDA" H 9255 3377 50  0000 C CNN
F 2 "" H 9250 3550 50  0001 C CNN
F 3 "" H 9250 3550 50  0001 C CNN
	1    9250 3550
	1    0    0    -1  
$EndComp
Text GLabel 7850 2000 2    50   Input ~ 0
VDDA1P3_BB
Wire Wire Line
	6250 1900 6350 1900
Wire Wire Line
	6350 1900 6350 2000
$Comp
L Device:Crystal Y1
U 1 1 5E3B8FFA
P 5700 3000
F 0 "Y1" H 5700 3268 50  0000 C CNN
F 1 "1.8 K OHM at 100MHz" H 5700 3177 50  0000 C CNN
F 2 "" H 5700 3000 50  0001 C CNN
F 3 "~" H 5700 3000 50  0001 C CNN
	1    5700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2000 4850 3000
Wire Wire Line
	6850 2100 6850 2000
Connection ~ 6850 2000
Wire Wire Line
	7250 2100 7250 2000
Wire Wire Line
	7650 2100 7650 2000
Wire Wire Line
	8700 3350 8700 3450
Wire Wire Line
	9600 3450 9600 3350
Wire Wire Line
	9150 3350 9150 3450
Connection ~ 6350 2000
Wire Wire Line
	6350 2000 6850 2000
Wire Wire Line
	5850 2000 6350 2000
Connection ~ 7250 2000
Wire Wire Line
	6850 2000 7250 2000
Connection ~ 9250 3450
Connection ~ 7650 2000
Wire Wire Line
	7650 2000 7850 2000
Wire Wire Line
	7250 2000 7650 2000
Wire Wire Line
	5550 3000 4850 3000
Connection ~ 4850 3000
Wire Wire Line
	4850 3000 4850 3500
Wire Wire Line
	5850 3000 6350 3000
$Comp
L Connector:TestPoint TP4
U 1 1 5E3D855F
P 6250 2900
F 0 "TP4" V 6353 2972 50  0000 C CNN
F 1 "TestPoint" H 6308 2927 50  0001 L CNN
F 2 "" H 6450 2900 50  0001 C CNN
F 3 "~" H 6450 2900 50  0001 C CNN
	1    6250 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 2900 6350 2900
Wire Wire Line
	6350 2900 6350 3000
Connection ~ 6350 3000
$Comp
L Device:C C60
U 1 1 5E3DD482
P 10050 3200
F 0 "C60" H 10165 3246 50  0000 L CNN
F 1 "1UF" H 10165 3155 50  0000 L CNN
F 2 "" H 10088 3050 50  0001 C CNN
F 3 "~" H 10050 3200 50  0001 C CNN
	1    10050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3550 9250 3450
Wire Wire Line
	9250 3450 9600 3450
Wire Wire Line
	10050 3450 10050 3350
Wire Wire Line
	8650 3000 8700 3000
Wire Wire Line
	8700 3000 8700 3050
Wire Wire Line
	9150 3000 9150 3050
Wire Wire Line
	10050 3000 10050 3050
Text GLabel 10050 3000 2    50   Input ~ 0
VDDA1P3_RF_SYNTH
$Comp
L Device:R R33
U 1 1 5E3E8E10
P 5700 3500
F 0 "R33" V 5907 3500 50  0000 C CNN
F 1 "0" V 5816 3500 50  0000 C CNN
F 2 "" V 5630 3500 50  0001 C CNN
F 3 "~" H 5700 3500 50  0001 C CNN
	1    5700 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3500 4850 3500
Connection ~ 4850 3500
Wire Wire Line
	4850 3500 4850 4500
$Comp
L Device:C C61
U 1 1 5E3ECF23
P 6850 2250
F 0 "C61" H 6965 2296 50  0000 L CNN
F 1 "10UF" H 6965 2205 50  0000 L CNN
F 2 "" H 6888 2100 50  0001 C CNN
F 3 "~" H 6850 2250 50  0001 C CNN
	1    6850 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C63
U 1 1 5E3ED3CD
P 7250 2250
F 0 "C63" H 7365 2296 50  0000 L CNN
F 1 "0.1UF" H 7350 2200 50  0000 L CNN
F 2 "" H 7288 2100 50  0001 C CNN
F 3 "~" H 7250 2250 50  0001 C CNN
	1    7250 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C69
U 1 1 5E3ED651
P 7650 2250
F 0 "C69" H 7765 2296 50  0000 L CNN
F 1 "1UF" H 7765 2205 50  0000 L CNN
F 2 "" H 7688 2100 50  0001 C CNN
F 3 "~" H 7650 2250 50  0001 C CNN
	1    7650 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2400 6850 2500
Wire Wire Line
	6850 2500 7250 2500
Wire Wire Line
	7650 2500 7650 2400
$Comp
L power:GNDA #PWR041
U 1 1 5E3EE5D5
P 7450 2550
F 0 "#PWR041" H 7450 2300 50  0001 C CNN
F 1 "GNDA" H 7455 2377 50  0000 C CNN
F 2 "" H 7450 2550 50  0001 C CNN
F 3 "" H 7450 2550 50  0001 C CNN
	1    7450 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2400 7250 2500
Connection ~ 7250 2500
Wire Wire Line
	7250 2500 7450 2500
Wire Wire Line
	7450 2550 7450 2500
Connection ~ 7450 2500
Wire Wire Line
	7450 2500 7650 2500
$Comp
L Device:C C68
U 1 1 5E3F6046
P 7450 3650
F 0 "C68" H 7565 3696 50  0000 L CNN
F 1 "0.1UF" H 7565 3605 50  0000 L CNN
F 2 "" H 7488 3500 50  0001 C CNN
F 3 "~" H 7450 3650 50  0001 C CNN
	1    7450 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C71
U 1 1 5E3F6050
P 7850 3650
F 0 "C71" H 7965 3696 50  0000 L CNN
F 1 "1UF" H 7965 3605 50  0000 L CNN
F 2 "" H 7888 3500 50  0001 C CNN
F 3 "~" H 7850 3650 50  0001 C CNN
	1    7850 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3900 7250 3900
Wire Wire Line
	7850 3900 7850 3800
$Comp
L power:GNDA #PWR039
U 1 1 5E3F605D
P 7250 4000
F 0 "#PWR039" H 7250 3750 50  0001 C CNN
F 1 "GNDA" H 7255 3827 50  0000 C CNN
F 2 "" H 7250 4000 50  0001 C CNN
F 3 "" H 7250 4000 50  0001 C CNN
	1    7250 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3800 7450 3900
Connection ~ 7450 3900
$Comp
L Connector:TestPoint TP5
U 1 1 5E3FEA67
P 6250 3450
F 0 "TP5" V 6445 3522 50  0000 C CNN
F 1 "TestPoint" V 6354 3522 50  0000 C CNN
F 2 "" H 6450 3450 50  0001 C CNN
F 3 "~" H 6450 3450 50  0001 C CNN
	1    6250 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 3500 6350 3500
Connection ~ 7050 3500
Wire Wire Line
	7050 3500 7450 3500
Connection ~ 7450 3500
Wire Wire Line
	7450 3500 7850 3500
Connection ~ 7850 3500
Wire Wire Line
	6250 3450 6350 3450
Wire Wire Line
	6350 3450 6350 3500
Connection ~ 6350 3500
Wire Wire Line
	6350 3500 7050 3500
Wire Wire Line
	7050 3800 7050 3900
$Comp
L Device:C C62
U 1 1 5E3F603C
P 7050 3650
F 0 "C62" H 7165 3696 50  0000 L CNN
F 1 "10UF" H 7165 3605 50  0000 L CNN
F 2 "" H 7088 3500 50  0001 C CNN
F 3 "~" H 7050 3650 50  0001 C CNN
	1    7050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4000 7250 3900
Connection ~ 7250 3900
Wire Wire Line
	7250 3900 7450 3900
Wire Wire Line
	7450 3900 7850 3900
Text GLabel 8150 3600 2    50   Input ~ 0
VDDA1P3_CLOCK_VCO_LDO
$Comp
L Device:R R34
U 1 1 5E409AD9
P 5700 4500
F 0 "R34" V 5907 4500 50  0000 C CNN
F 1 "0" V 5816 4500 50  0000 C CNN
F 2 "" V 5630 4500 50  0001 C CNN
F 3 "~" H 5700 4500 50  0001 C CNN
	1    5700 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 4500 4850 4500
Connection ~ 4850 4500
Wire Wire Line
	4850 4500 4850 5000
Wire Wire Line
	5850 4500 6350 4500
$Comp
L Connector:TestPoint TP6
U 1 1 5E40F1B6
P 6250 4400
F 0 "TP6" V 6353 4472 50  0000 C CNN
F 1 "TestPoint" H 6308 4427 50  0001 L CNN
F 2 "" H 6450 4400 50  0001 C CNN
F 3 "~" H 6450 4400 50  0001 C CNN
	1    6250 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 4400 6350 4400
Wire Wire Line
	6350 4400 6350 4500
Connection ~ 6350 4500
Wire Wire Line
	6350 4500 8850 4500
$Comp
L Device:R R42
U 1 1 5E4127E9
P 9000 4500
F 0 "R42" V 9207 4500 50  0000 C CNN
F 1 "0" V 9116 4500 50  0000 C CNN
F 2 "" V 8930 4500 50  0001 C CNN
F 3 "~" H 9000 4500 50  0001 C CNN
	1    9000 4500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C78
U 1 1 5E412F6D
P 9350 4650
F 0 "C78" H 9465 4696 50  0000 L CNN
F 1 "10UF" H 9465 4605 50  0000 L CNN
F 2 "" H 9388 4500 50  0001 C CNN
F 3 "~" H 9350 4650 50  0001 C CNN
	1    9350 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C81
U 1 1 5E413059
P 9750 4650
F 0 "C81" H 9865 4696 50  0000 L CNN
F 1 "1UF" H 9865 4605 50  0000 L CNN
F 2 "" H 9788 4500 50  0001 C CNN
F 3 "~" H 9750 4650 50  0001 C CNN
	1    9750 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C86
U 1 1 5E413243
P 10150 4650
F 0 "C86" H 10265 4696 50  0000 L CNN
F 1 "0.1UF" H 10265 4605 50  0000 L CNN
F 2 "" H 10188 4500 50  0001 C CNN
F 3 "~" H 10150 4650 50  0001 C CNN
	1    10150 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4500 9350 4500
Wire Wire Line
	9350 4500 9750 4500
Connection ~ 9350 4500
Wire Wire Line
	9750 4500 10150 4500
Connection ~ 9750 4500
Wire Wire Line
	9350 4800 9350 4850
Wire Wire Line
	9350 4850 9700 4850
Wire Wire Line
	10150 4850 10150 4800
Wire Wire Line
	9750 4800 9750 4850
Connection ~ 9750 4850
Wire Wire Line
	9750 4850 10150 4850
$Comp
L power:GNDA #PWR045
U 1 1 5E41D5F9
P 9700 4900
F 0 "#PWR045" H 9700 4650 50  0001 C CNN
F 1 "GNDA" H 9705 4727 50  0000 C CNN
F 2 "" H 9700 4900 50  0001 C CNN
F 3 "" H 9700 4900 50  0001 C CNN
	1    9700 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 4900 9700 4850
Connection ~ 9700 4850
Wire Wire Line
	9700 4850 9750 4850
Wire Wire Line
	10150 4500 10350 4500
Connection ~ 10150 4500
Text GLabel 10350 4500 2    50   Input ~ 0
VDDA1P3_RX_RF
$Comp
L Device:R R35
U 1 1 5E422FDA
P 5700 5000
F 0 "R35" V 5907 5000 50  0000 C CNN
F 1 "0" V 5816 5000 50  0000 C CNN
F 2 "" V 5630 5000 50  0001 C CNN
F 3 "~" H 5700 5000 50  0001 C CNN
	1    5700 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 5000 4850 5000
Connection ~ 4850 5000
Wire Wire Line
	4850 5000 4850 5500
$Comp
L Device:C C64
U 1 1 5E426272
P 7350 5150
F 0 "C64" H 7465 5196 50  0000 L CNN
F 1 "10UF" H 7465 5105 50  0000 L CNN
F 2 "" H 7388 5000 50  0001 C CNN
F 3 "~" H 7350 5150 50  0001 C CNN
	1    7350 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5E42976C
P 6250 4900
F 0 "TP7" V 6353 4972 50  0000 C CNN
F 1 "TestPoint" H 6308 4927 50  0001 L CNN
F 2 "" H 6450 4900 50  0001 C CNN
F 3 "~" H 6450 4900 50  0001 C CNN
	1    6250 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 4900 6350 4900
Wire Wire Line
	6350 4900 6350 5000
Wire Wire Line
	5850 5000 6350 5000
Connection ~ 6350 5000
$Comp
L Device:C C72
U 1 1 5E42CAC7
P 7850 5150
F 0 "C72" H 7965 5196 50  0000 L CNN
F 1 "1UF" H 7965 5105 50  0000 L CNN
F 2 "" H 7888 5000 50  0001 C CNN
F 3 "~" H 7850 5150 50  0001 C CNN
	1    7850 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5000 7350 5000
Wire Wire Line
	7350 5000 7850 5000
Connection ~ 7350 5000
Wire Wire Line
	7850 5000 8250 5000
Connection ~ 7850 5000
Wire Wire Line
	7350 5300 7350 5350
Wire Wire Line
	7350 5350 7600 5350
Wire Wire Line
	7850 5350 7850 5300
$Comp
L power:GNDA #PWR042
U 1 1 5E4361A4
P 7600 5500
F 0 "#PWR042" H 7600 5250 50  0001 C CNN
F 1 "GNDA" H 7605 5327 50  0000 C CNN
F 2 "" H 7600 5500 50  0001 C CNN
F 3 "" H 7600 5500 50  0001 C CNN
	1    7600 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 5500 7600 5350
Connection ~ 7600 5350
Wire Wire Line
	7600 5350 7850 5350
Text GLabel 8250 5000 2    50   Input ~ 0
VDDA1P3_CLOCK_SYNTH
Wire Wire Line
	4850 5500 4850 6000
Connection ~ 4850 5500
$Comp
L Device:R R36
U 1 1 5E447E4D
P 5700 6000
F 0 "R36" V 5907 6000 50  0000 C CNN
F 1 "0" V 5816 6000 50  0000 C CNN
F 2 "" V 5630 6000 50  0001 C CNN
F 3 "~" H 5700 6000 50  0001 C CNN
	1    5700 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 6000 4850 6000
Wire Wire Line
	5850 6000 6350 6000
$Comp
L Connector:TestPoint TP8
U 1 1 5E44E8CB
P 6250 5900
F 0 "TP8" V 6353 5972 50  0000 C CNN
F 1 "TestPoint" H 6308 5927 50  0001 L CNN
F 2 "" H 6450 5900 50  0001 C CNN
F 3 "~" H 6450 5900 50  0001 C CNN
	1    6250 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 5900 6350 5900
Wire Wire Line
	6350 5900 6350 6000
Connection ~ 6350 6000
Wire Wire Line
	6350 6000 9250 6000
$Comp
L Device:C C79
U 1 1 5E45491C
P 9250 6150
F 0 "C79" H 9365 6196 50  0000 L CNN
F 1 "10UF" H 9365 6105 50  0000 L CNN
F 2 "" H 9288 6000 50  0001 C CNN
F 3 "~" H 9250 6150 50  0001 C CNN
	1    9250 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C82
U 1 1 5E454926
P 9700 6150
F 0 "C82" H 9815 6196 50  0000 L CNN
F 1 "0.1UF" H 9815 6105 50  0000 L CNN
F 2 "" H 9738 6000 50  0001 C CNN
F 3 "~" H 9700 6150 50  0001 C CNN
	1    9700 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C87
U 1 1 5E454930
P 10150 6150
F 0 "C87" H 10265 6196 50  0000 L CNN
F 1 "1UF" H 10265 6105 50  0000 L CNN
F 2 "" H 10188 6000 50  0001 C CNN
F 3 "~" H 10150 6150 50  0001 C CNN
	1    10150 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 6350 10150 6300
$Comp
L power:GNDA #PWR046
U 1 1 5E454940
P 9700 6400
F 0 "#PWR046" H 9700 6150 50  0001 C CNN
F 1 "GNDA" H 9705 6227 50  0000 C CNN
F 2 "" H 9700 6400 50  0001 C CNN
F 3 "" H 9700 6400 50  0001 C CNN
	1    9700 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 6400 9700 6350
Connection ~ 9700 6350
Connection ~ 10150 6000
Wire Wire Line
	10150 6000 10350 6000
Text GLabel 10350 6000 2    50   Input ~ 0
VDDA1P3_RX_TX
Wire Wire Line
	4850 6000 4850 6500
Connection ~ 4850 6000
$Comp
L Device:R R37
U 1 1 5E467E59
P 5700 6500
F 0 "R37" V 5907 6500 50  0000 C CNN
F 1 "0" V 5816 6500 50  0000 C CNN
F 2 "" V 5630 6500 50  0001 C CNN
F 3 "~" H 5700 6500 50  0001 C CNN
	1    5700 6500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 6500 4850 6500
Wire Wire Line
	5850 6500 6350 6500
$Comp
L Connector:TestPoint TP9
U 1 1 5E46F125
P 6250 6400
F 0 "TP9" V 6353 6472 50  0000 C CNN
F 1 "TestPoint" H 6308 6427 50  0001 L CNN
F 2 "" H 6450 6400 50  0001 C CNN
F 3 "~" H 6450 6400 50  0001 C CNN
	1    6250 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 6400 6350 6400
Wire Wire Line
	6350 6400 6350 6500
Connection ~ 6350 6500
Wire Wire Line
	6350 6500 7350 6500
$Comp
L Device:C C65
U 1 1 5E474CA9
P 7350 6650
F 0 "C65" H 7465 6696 50  0000 L CNN
F 1 "10UF" H 7465 6605 50  0000 L CNN
F 2 "" H 7388 6500 50  0001 C CNN
F 3 "~" H 7350 6650 50  0001 C CNN
	1    7350 6650
	1    0    0    -1  
$EndComp
Connection ~ 7350 6500
Wire Wire Line
	7350 6500 7850 6500
$Comp
L Device:C C73
U 1 1 5E475F99
P 7850 6650
F 0 "C73" H 7965 6696 50  0000 L CNN
F 1 "0.1UF" H 7965 6605 50  0000 L CNN
F 2 "" H 7888 6500 50  0001 C CNN
F 3 "~" H 7850 6650 50  0001 C CNN
	1    7850 6650
	1    0    0    -1  
$EndComp
Connection ~ 7850 6500
Wire Wire Line
	7850 6500 8350 6500
$Comp
L Device:C C76
U 1 1 5E47628F
P 8350 6650
F 0 "C76" H 8465 6696 50  0000 L CNN
F 1 "1UF" H 8465 6605 50  0000 L CNN
F 2 "" H 8388 6500 50  0001 C CNN
F 3 "~" H 8350 6650 50  0001 C CNN
	1    8350 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 6800 7350 6900
Wire Wire Line
	7350 6900 7850 6900
Wire Wire Line
	8350 6900 8350 6800
Wire Wire Line
	7850 6800 7850 6900
Connection ~ 7850 6900
Wire Wire Line
	7850 6900 8350 6900
$Comp
L power:GNDA #PWR043
U 1 1 5E47F6B5
P 7850 7000
F 0 "#PWR043" H 7850 6750 50  0001 C CNN
F 1 "GNDA" H 7855 6827 50  0000 C CNN
F 2 "" H 7850 7000 50  0001 C CNN
F 3 "" H 7850 7000 50  0001 C CNN
	1    7850 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 7000 7850 6900
Text GLabel 8550 6500 2    50   Input ~ 0
VDDA1P3_AUX_VCO_LDO
Wire Wire Line
	8550 6500 8350 6500
Connection ~ 8350 6500
Connection ~ 4850 6500
Wire Wire Line
	4850 6500 4850 7500
$Comp
L Device:R R38
U 1 1 5E49B800
P 5700 7500
F 0 "R38" V 5907 7500 50  0000 C CNN
F 1 "0" V 5816 7500 50  0000 C CNN
F 2 "" V 5630 7500 50  0001 C CNN
F 3 "~" H 5700 7500 50  0001 C CNN
	1    5700 7500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 7500 4850 7500
Wire Wire Line
	5850 7500 6350 7500
$Comp
L Connector:TestPoint TP10
U 1 1 5E4A7399
P 6250 7400
F 0 "TP10" V 6353 7472 50  0000 C CNN
F 1 "TestPoint" H 6308 7427 50  0001 L CNN
F 2 "" H 6450 7400 50  0001 C CNN
F 3 "~" H 6450 7400 50  0001 C CNN
	1    6250 7400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 7400 6350 7400
Wire Wire Line
	6350 7400 6350 7500
Connection ~ 6350 7500
Wire Wire Line
	6350 7500 9050 7500
$Comp
L Device:C C80
U 1 1 5E4ADAA2
P 9600 7700
F 0 "C80" H 9715 7746 50  0000 L CNN
F 1 "220UF" H 9715 7655 50  0000 L CNN
F 2 "" H 9638 7550 50  0001 C CNN
F 3 "~" H 9600 7700 50  0001 C CNN
	1    9600 7700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C84
U 1 1 5E4ADD07
P 10050 7700
F 0 "C84" H 10165 7746 50  0000 L CNN
F 1 "220UF" H 10165 7655 50  0000 L CNN
F 2 "" H 10088 7550 50  0001 C CNN
F 3 "~" H 10050 7700 50  0001 C CNN
	1    10050 7700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C90
U 1 1 5E4ADE4B
P 10500 7700
F 0 "C90" H 10615 7746 50  0000 L CNN
F 1 "220UF" H 10615 7655 50  0000 L CNN
F 2 "" H 10538 7550 50  0001 C CNN
F 3 "~" H 10500 7700 50  0001 C CNN
	1    10500 7700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C93
U 1 1 5E4AE348
P 10950 7700
F 0 "C93" H 11065 7746 50  0000 L CNN
F 1 "1UF" H 11065 7655 50  0000 L CNN
F 2 "" H 10988 7550 50  0001 C CNN
F 3 "~" H 10950 7700 50  0001 C CNN
	1    10950 7700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R43
U 1 1 5E4B03FC
P 9200 7500
F 0 "R43" V 9407 7500 50  0000 C CNN
F 1 "0.1" V 9316 7500 50  0000 C CNN
F 2 "" V 9130 7500 50  0001 C CNN
F 3 "~" H 9200 7500 50  0001 C CNN
	1    9200 7500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9600 7500 9600 7550
Wire Wire Line
	9350 7500 9600 7500
Wire Wire Line
	9600 7500 10050 7500
Wire Wire Line
	10050 7500 10050 7550
Connection ~ 9600 7500
Wire Wire Line
	10050 7500 10500 7500
Wire Wire Line
	10500 7500 10500 7550
Connection ~ 10050 7500
Wire Wire Line
	10500 7500 10950 7500
Wire Wire Line
	10950 7500 10950 7550
Connection ~ 10500 7500
Wire Wire Line
	9600 7850 9600 7950
Wire Wire Line
	9600 7950 10050 7950
Wire Wire Line
	10950 7950 10950 7850
Wire Wire Line
	10500 7850 10500 7950
Connection ~ 10500 7950
Wire Wire Line
	10500 7950 10950 7950
Wire Wire Line
	10050 7850 10050 7950
Connection ~ 10050 7950
Text GLabel 11350 7500 2    50   Input ~ 0
VDDA1P3_RF_LO
Wire Wire Line
	11350 7500 10950 7500
Connection ~ 10950 7500
Wire Wire Line
	4850 7500 4850 8500
Connection ~ 4850 7500
$Comp
L Device:R R39
U 1 1 5E4F19B5
P 5700 8500
F 0 "R39" V 5907 8500 50  0000 C CNN
F 1 "0" V 5816 8500 50  0000 C CNN
F 2 "" V 5630 8500 50  0001 C CNN
F 3 "~" H 5700 8500 50  0001 C CNN
	1    5700 8500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 8500 4850 8500
Wire Wire Line
	5850 8500 6350 8500
$Comp
L Connector:TestPoint TP11
U 1 1 5E4F19C1
P 6250 8400
F 0 "TP11" V 6353 8472 50  0000 C CNN
F 1 "TestPoint" H 6308 8427 50  0001 L CNN
F 2 "" H 6450 8400 50  0001 C CNN
F 3 "~" H 6450 8400 50  0001 C CNN
	1    6250 8400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 8400 6350 8400
Wire Wire Line
	6350 8400 6350 8500
Connection ~ 6350 8500
Wire Wire Line
	6350 8500 7350 8500
$Comp
L Device:C C66
U 1 1 5E4F19CF
P 7350 8650
F 0 "C66" H 7465 8696 50  0000 L CNN
F 1 "10UF" H 7465 8605 50  0000 L CNN
F 2 "" H 7388 8500 50  0001 C CNN
F 3 "~" H 7350 8650 50  0001 C CNN
	1    7350 8650
	1    0    0    -1  
$EndComp
Connection ~ 7350 8500
Wire Wire Line
	7350 8500 7850 8500
$Comp
L Device:C C74
U 1 1 5E4F19DB
P 7850 8650
F 0 "C74" H 7965 8696 50  0000 L CNN
F 1 "0.1UF" H 7965 8605 50  0000 L CNN
F 2 "" H 7888 8500 50  0001 C CNN
F 3 "~" H 7850 8650 50  0001 C CNN
	1    7850 8650
	1    0    0    -1  
$EndComp
Connection ~ 7850 8500
Wire Wire Line
	7850 8500 8350 8500
$Comp
L Device:C C77
U 1 1 5E4F19E7
P 8350 8650
F 0 "C77" H 8465 8696 50  0000 L CNN
F 1 "1UF" H 8465 8605 50  0000 L CNN
F 2 "" H 8388 8500 50  0001 C CNN
F 3 "~" H 8350 8650 50  0001 C CNN
	1    8350 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 8800 7350 8900
Wire Wire Line
	7350 8900 7850 8900
Wire Wire Line
	8350 8900 8350 8800
Wire Wire Line
	7850 8800 7850 8900
Connection ~ 7850 8900
Wire Wire Line
	7850 8900 8350 8900
$Comp
L power:GNDA #PWR044
U 1 1 5E4F19F7
P 7850 9000
F 0 "#PWR044" H 7850 8750 50  0001 C CNN
F 1 "GNDA" H 7855 8827 50  0000 C CNN
F 2 "" H 7850 9000 50  0001 C CNN
F 3 "" H 7850 9000 50  0001 C CNN
	1    7850 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 9000 7850 8900
Text GLabel 8550 8500 2    50   Input ~ 0
VDDA1P3_AUX_VCO_LDO
Wire Wire Line
	8550 8500 8350 8500
Connection ~ 8350 8500
Wire Wire Line
	4850 8500 4850 9500
Connection ~ 4850 8500
$Comp
L Device:R R40
U 1 1 5E5175D9
P 5700 9500
F 0 "R40" V 5907 9500 50  0000 C CNN
F 1 "0" V 5816 9500 50  0000 C CNN
F 2 "" V 5630 9500 50  0001 C CNN
F 3 "~" H 5700 9500 50  0001 C CNN
	1    5700 9500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 9500 5550 9500
Wire Wire Line
	5850 9500 6350 9500
$Comp
L Connector:TestPoint TP12
U 1 1 5E527755
P 6250 9400
F 0 "TP12" V 6353 9472 50  0000 C CNN
F 1 "TestPoint" H 6308 9427 50  0001 L CNN
F 2 "" H 6450 9400 50  0001 C CNN
F 3 "~" H 6450 9400 50  0001 C CNN
	1    6250 9400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 9400 6350 9400
Wire Wire Line
	6350 9400 6350 9500
Connection ~ 6350 9500
Wire Wire Line
	6350 9500 9950 9500
$Comp
L Device:C C85
U 1 1 5E535A5D
P 9950 9650
F 0 "C85" H 10065 9696 50  0000 L CNN
F 1 "10UF" H 10065 9605 50  0000 L CNN
F 2 "" H 9988 9500 50  0001 C CNN
F 3 "~" H 9950 9650 50  0001 C CNN
	1    9950 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C89
U 1 1 5E535A67
P 10400 9650
F 0 "C89" H 10515 9696 50  0000 L CNN
F 1 "0.1UF" H 10515 9605 50  0000 L CNN
F 2 "" H 10438 9500 50  0001 C CNN
F 3 "~" H 10400 9650 50  0001 C CNN
	1    10400 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C92
U 1 1 5E535A71
P 10850 9650
F 0 "C92" H 10965 9696 50  0000 L CNN
F 1 "1UF" H 10965 9605 50  0000 L CNN
F 2 "" H 10888 9500 50  0001 C CNN
F 3 "~" H 10850 9650 50  0001 C CNN
	1    10850 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 9800 9950 9900
Wire Wire Line
	10850 9900 10850 9800
$Comp
L power:GNDA #PWR049
U 1 1 5E535A7E
P 10650 9950
F 0 "#PWR049" H 10650 9700 50  0001 C CNN
F 1 "GNDA" H 10655 9777 50  0000 C CNN
F 2 "" H 10650 9950 50  0001 C CNN
F 3 "" H 10650 9950 50  0001 C CNN
	1    10650 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 9950 10650 9900
Connection ~ 10650 9900
Wire Wire Line
	10650 9900 10850 9900
Connection ~ 10850 9500
Wire Wire Line
	10850 9500 11050 9500
Wire Wire Line
	10050 7950 10450 7950
$Comp
L power:GNDA #PWR047
U 1 1 5E57ABFC
P 10450 8150
F 0 "#PWR047" H 10450 7900 50  0001 C CNN
F 1 "GNDA" H 10455 7977 50  0000 C CNN
F 2 "" H 10450 8150 50  0001 C CNN
F 3 "" H 10450 8150 50  0001 C CNN
	1    10450 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 8150 10450 7950
Connection ~ 10450 7950
Wire Wire Line
	10450 7950 10500 7950
Wire Wire Line
	4850 9500 4850 10000
Connection ~ 4850 9500
$Comp
L Device:R R41
U 1 1 5E58E731
P 5700 10500
F 0 "R41" V 5907 10500 50  0000 C CNN
F 1 "0" V 5816 10500 50  0000 C CNN
F 2 "" V 5630 10500 50  0001 C CNN
F 3 "~" H 5700 10500 50  0001 C CNN
	1    5700 10500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 10500 4850 10500
Wire Wire Line
	5850 10500 6350 10500
$Comp
L Connector:TestPoint TP14
U 1 1 5E5A0EAF
P 6250 10400
F 0 "TP14" V 6353 10472 50  0000 C CNN
F 1 "TestPoint" H 6308 10427 50  0001 L CNN
F 2 "" H 6450 10400 50  0001 C CNN
F 3 "~" H 6450 10400 50  0001 C CNN
	1    6250 10400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 10400 6350 10400
Wire Wire Line
	6350 10400 6350 10500
Connection ~ 6350 10500
Wire Wire Line
	6350 10500 9900 10500
$Comp
L Device:C C83
U 1 1 5E5AD21D
P 9900 10650
F 0 "C83" H 10015 10696 50  0000 L CNN
F 1 "10UF" H 10015 10605 50  0000 L CNN
F 2 "" H 9938 10500 50  0001 C CNN
F 3 "~" H 9900 10650 50  0001 C CNN
	1    9900 10650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C88
U 1 1 5E5AD227
P 10300 10650
F 0 "C88" H 10415 10696 50  0000 L CNN
F 1 "0.1UF" H 10415 10605 50  0000 L CNN
F 2 "" H 10338 10500 50  0001 C CNN
F 3 "~" H 10300 10650 50  0001 C CNN
	1    10300 10650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C91
U 1 1 5E5AD231
P 10750 10650
F 0 "C91" H 10865 10696 50  0000 L CNN
F 1 "1UF" H 10865 10605 50  0000 L CNN
F 2 "" H 10788 10500 50  0001 C CNN
F 3 "~" H 10750 10650 50  0001 C CNN
	1    10750 10650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 10800 9900 10900
Wire Wire Line
	10750 10900 10750 10800
$Comp
L power:GNDA #PWR048
U 1 1 5E5AD23E
P 10550 10950
F 0 "#PWR048" H 10550 10700 50  0001 C CNN
F 1 "GNDA" H 10555 10777 50  0000 C CNN
F 2 "" H 10550 10950 50  0001 C CNN
F 3 "" H 10550 10950 50  0001 C CNN
	1    10550 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 10800 10300 10900
Wire Wire Line
	10550 10950 10550 10900
Connection ~ 10550 10900
Wire Wire Line
	10550 10900 10750 10900
Wire Wire Line
	10750 10500 11050 10500
Connection ~ 10750 10500
Text GLabel 11050 10500 2    50   Input ~ 0
VDDA1P3_DES
Text GLabel 11050 9500 2    50   Input ~ 0
VDDA1P3_AUX_SYNTH
$Comp
L Device:Crystal Y3
U 1 1 5E60559F
P 5700 10000
F 0 "Y3" H 5700 10268 50  0000 C CNN
F 1 "Crystal" H 5700 10177 50  0000 C CNN
F 2 "" H 5700 10000 50  0001 C CNN
F 3 "~" H 5700 10000 50  0001 C CNN
	1    5700 10000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 10000 4850 10000
Connection ~ 4850 10000
Wire Wire Line
	4850 10000 4850 10500
Wire Wire Line
	5850 10000 6350 10000
$Comp
L Connector:TestPoint TP13
U 1 1 5E61AC2A
P 6250 9900
F 0 "TP13" V 6353 9972 50  0000 C CNN
F 1 "TestPoint" H 6308 9927 50  0001 L CNN
F 2 "" H 6450 9900 50  0001 C CNN
F 3 "~" H 6450 9900 50  0001 C CNN
	1    6250 9900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 9900 6350 9900
Wire Wire Line
	6350 9900 6350 10000
Connection ~ 6350 10000
Wire Wire Line
	6350 10000 7300 10000
$Comp
L Device:C C67
U 1 1 5E626F40
P 7300 10150
F 0 "C67" H 7415 10196 50  0000 L CNN
F 1 "0.1UF" H 7415 10105 50  0000 L CNN
F 2 "" H 7338 10000 50  0001 C CNN
F 3 "~" H 7300 10150 50  0001 C CNN
	1    7300 10150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C70
U 1 1 5E626F4A
P 7750 10150
F 0 "C70" H 7865 10196 50  0000 L CNN
F 1 "1UF" H 7865 10105 50  0000 L CNN
F 2 "" H 7788 10000 50  0001 C CNN
F 3 "~" H 7750 10150 50  0001 C CNN
	1    7750 10150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C75
U 1 1 5E626F54
P 8150 10150
F 0 "C75" H 8265 10196 50  0000 L CNN
F 1 "0.1UF" H 8265 10105 50  0000 L CNN
F 2 "" H 8188 10000 50  0001 C CNN
F 3 "~" H 8150 10150 50  0001 C CNN
	1    8150 10150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR040
U 1 1 5E626F61
P 7300 10300
F 0 "#PWR040" H 7300 10050 50  0001 C CNN
F 1 "GNDA" H 7305 10127 50  0000 C CNN
F 2 "" H 7300 10300 50  0001 C CNN
F 3 "" H 7300 10300 50  0001 C CNN
	1    7300 10300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 10300 8150 10300
Connection ~ 7750 10300
Wire Wire Line
	7750 10000 8150 10000
Connection ~ 7750 10000
Text GLabel 8350 10000 2    50   Input ~ 0
VDDA1P3_SER
Wire Wire Line
	8350 10000 8150 10000
Connection ~ 8150 10000
Connection ~ 9250 6000
Wire Wire Line
	9700 6350 9700 6300
Wire Wire Line
	9250 6350 9250 6300
Wire Wire Line
	9250 6350 9700 6350
Wire Wire Line
	9700 6350 10150 6350
Wire Wire Line
	9250 6000 9700 6000
Connection ~ 9700 6000
Wire Wire Line
	9700 6000 10150 6000
Connection ~ 9950 9500
Wire Wire Line
	9950 9500 10400 9500
Connection ~ 10400 9500
Wire Wire Line
	10400 9500 10850 9500
Wire Wire Line
	9950 9900 10400 9900
Wire Wire Line
	10400 9800 10400 9900
Connection ~ 10400 9900
Wire Wire Line
	10400 9900 10650 9900
Wire Wire Line
	6350 3000 8350 3000
$Comp
L Device:Crystal Y2
U 1 1 5E3D9A12
P 8500 3000
F 0 "Y2" H 8500 3268 50  0000 C CNN
F 1 "Crystal" H 8500 3177 50  0000 C CNN
F 2 "" H 8500 3000 50  0001 C CNN
F 3 "~" H 8500 3000 50  0001 C CNN
	1    8500 3000
	1    0    0    -1  
$EndComp
Connection ~ 9150 3450
Wire Wire Line
	9150 3450 9250 3450
Wire Wire Line
	8700 3450 9150 3450
Connection ~ 8700 3000
Wire Wire Line
	8700 3000 9150 3000
Connection ~ 9150 3000
Connection ~ 9600 3450
Wire Wire Line
	9600 3450 10050 3450
Wire Wire Line
	9600 3050 9600 3000
Wire Wire Line
	9150 3000 9600 3000
Connection ~ 9600 3000
Wire Wire Line
	9600 3000 10050 3000
Wire Wire Line
	8150 3500 8150 3600
Wire Wire Line
	7850 3500 8150 3500
Connection ~ 7300 10000
Wire Wire Line
	7300 10000 7750 10000
Connection ~ 7300 10300
Connection ~ 9900 10500
Connection ~ 10300 10500
Wire Wire Line
	9900 10500 10300 10500
Wire Wire Line
	10300 10500 10750 10500
Wire Wire Line
	7300 10300 7750 10300
Wire Wire Line
	9900 10900 10300 10900
Connection ~ 10300 10900
Wire Wire Line
	10300 10900 10550 10900
$EndSCHEMATC
